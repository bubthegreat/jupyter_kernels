FROM ubuntu:xenial

LABEL maintainer="bubthegreat@gmail.com"
ENV PATH="${PATH}:/anaconda/bin:/usr/local/go/bin:/root/go/bin"

RUN apt-get update
RUN apt-get install -y \
    bash \
    build-essential \
    curl \
    git \
    libczmq-dev \
    pkg-config \
    software-properties-common \
    vim \
    wget

# Using anaconda because it's simpler for getting/maintaining dependencies.
RUN curl -O https://repo.continuum.io/archive/Anaconda3-2018.12-Linux-x86_64.sh
RUN bash Anaconda3-2018.12-Linux-x86_64.sh -b -p /anaconda
RUN rm Anaconda3-2018.12-Linux-x86_64.sh
RUN conda update conda
RUN conda install -y -c QuantStack -c \
    conda-forge \
    xeus-cling

# Install golang
RUN wget https://dl.google.com/go/go1.11.4.linux-amd64.tar.gz
RUN tar -xvf go1.11.4.linux-amd64.tar.gz
RUN mv go /usr/local
ENV GOROOT=/usr/local/go
ENV GOPATH=/root/go

# install gophernotes
RUN go get github.com/gopherdata/gophernotes
RUN mkdir -p ~/.local/share/jupyter/kernels/gophernotes
RUN cp $GOPATH/src/github.com/gopherdata/gophernotes/kernel/* ~/.local/share/jupyter/kernels/gophernotes

# Install bash kernel
RUN pip install bash_kernel
RUN python -m bash_kernel.install

# Installing brython magic for javascript via python
RUN pip install brythonmagic

# Install ijavascript kernel
RUN apt-get install -y nodejs-legacy npm
RUN npm install -g ijavascript
RUN ijsinstall

RUN apt-get install -y node
RUN npm install -g itypescript
RUN its --ts-install=global


# Cleanup
RUN rm go1.11.4.linux-amd64.tar.gz

RUN mkdir workspace

WORKDIR /workspace
