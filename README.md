# jupyter_kernels

Aims to simplify installing and/or testing new jupyter kernels.  For now, it's just a docker image with all the ones I've managed to install successfully.  Edit startup in docker-compose if you're using it to deploy a swarm service and want passwords, etc.

Image can be pulled from dockerhub rather than built:
```
docker pull bubthegreat/jupyter_kernels
```

To deploy it as a service on swarm:
```
docker stack deploy --compose-file docker-compose.yml
```